%define LIST_HEAD 0


%macro colon 2
    %2:
    dq LIST_HEAD  ; добавляем адрес следующего элемента
    db %1, 0 ; после - значение 
    %define LIST_HEAD %2

%endmacro