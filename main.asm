%include 'colon.inc'
%include 'words.inc'
%include 'lib.inc'
extern find_word
section .rodata
    too_long: db "too long", 0
    no_such_key: db "no such key", 0   



section .data
    %define BUF_SIZE 256

section .bss
    input_buffer: resb BUF_SIZE




section .text
global _start
_start:
    mov rdi, input_buffer
    push rdi
    
    call read_string
    pop rdi
    test rax, rax
    push rax

    jz .too_long
    

    
    mov rsi, LIST_HEAD
    call find_word
    test rax, rax
    jz .no_such_key
    jmp .print_by_key

    .print_by_key:

        mov rdi, rax
        add rdi, 8
        pop rax
        add rdi, rax
        
        call print_string
        call print_newline
        call exit

    .too_long:
        mov rdi, too_long
        call print_string_to_err
        call exit

    .no_such_key:
        mov rdi, no_such_key
        call print_string_to_err
        call exit
