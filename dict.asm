section .text

global find_word
%include 'lib.inc'
find_word:
;rdi - string, rsi - pointer to dictionary

    .loop:


        add rsi, 8 ;because of pointer to next elem

        push rdi
        push rsi
        
        call string_equals
        
        
        pop rsi
        pop rdi

        sub rsi, 8

        test rax, rax
        jnz .found


        mov rsi, [rsi] ;jump to next elem
        test rsi, rsi
        jnz .loop

   .end_of_dict:
        xor rax, rax
        ret
    .found:
        mov rax, rsi
        ret
 