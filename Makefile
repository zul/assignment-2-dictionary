lib.o: lib.asm 
	nasm -f elf64 -o lib.o lib.asm

dict.o: dict.asm
	nasm -f elf64 -o dict.o dict.asm

main.o: main.asm
	nasm -f elf64 -o main.o main.asm



main: lib.o dict.o main.o
	ld -o program main.o lib.o dict.o

.PHONY: clean
clean:
	rm -rf *.o main